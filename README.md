## Homework for pillat

Simple srapper for [bbc.com](http://bbc.com "bbc.com")

### Used technologies:
- aiohttp - as web-server framerwork
- pyyaml - lib for parsing **config.yaml** config file
- BeautifulSoup - lib for parsing html text

### How to run:
#### - With Docker:
	docker build -t pillar-homework -f Dockerfile .
	docker run -p 8080:8080 pillar-homework

#### - With Virtualenv
	python3 -m virtuelenv venv
	source venv/bin/activate
	pip install -r requirements.txt
	python ./pillar_homework/main.py

### Usage example:
```
curl -X GET 'http://127.0.0.1:8080?chapter=sport&news=10'
```
