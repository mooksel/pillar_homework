FROM python:3.7.3-alpine

EXPOSE 8080

WORKDIR /work

COPY config config
COPY pillar_homework pillar_homework
COPY requirements.txt /tmp/requirements.txt

RUN pip install --no-cache-dir -r /tmp/requirements.txt

CMD ["python", "pillar_homework/main.py"]
