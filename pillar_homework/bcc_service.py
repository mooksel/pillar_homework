from bs4 import BeautifulSoup


class BCCService:

    def __init__(self, bcc_url, client_session):
        self._bcc_url = bcc_url
        self._client_session = client_session

    @property
    def bcc_url(self):
        return self._bcc_url

    @property
    def client_session(self):
        return self._client_session

    async def _load_page_content(self, chapter_name):
        parse_url = f'{self.bcc_url}/{chapter_name}'

        async with self.client_session.get(parse_url) as response:
            if response.status != 200:
                return None

            page_content = await response.text()

        return page_content


    async def get_news(self, chapter_name, news_count):
        page_content = await self._load_page_content(chapter_name)

        if not page_content:
            return None

        soup = BeautifulSoup(page_content, 'html.parser')

        title = self._parse_title(soup)
        news = self._parse_news_items(soup, news_count)

        if title is None or news is None:
            return None

        return {
            'chapter': title,
            'news': news,
        }

    def _parse_title(self, soup):
        title_a = soup.find('a', {'class': 'sp-c-global-header__logo'})
        if not title_a:
            return None

        title_a_children = tuple(title_a.children)
        if len(title_a_children) < 2:
            return None

        return title_a_children[1]

    def _parse_news_items(self, soup, max_count):
        news_links = soup.find_all('a', class_='gs-c-promo-heading')

        if news_links is None:
            return None

        items = []
        if max_count == 0:
            return items

        if len(news_links) < max_count:
            max_count = len(news_links)

        for news_link in news_links:
            try:
                news_path = news_link.get('href')
                title = news_link.select('h3')[0].get_text()

                items.append({
                    'URL': f'{self.bcc_url}{news_path}',
                    'title': title,
                })
            except Exception:
                pass

            if len(items) == max_count:
                break

        return items

    # def _parse_chapter(self, html_text):
    #     return {
    #         'chapter' : 'sport',
    #         'news' : [
    #             {
    #                 'title' : 'Liam Williams: Full-back fit as Wales unchanged to face Ireland',
    #                 'URL' : 'https://www.bbc.com/sport/rugby-union/47568773'
    #             },
    #             {
    #                 'title' : 'Bayern Munich 1-3 Liverpool: Jurgen Klopp says Reds are among Europe\'s elite again',
    #                 'URL' : 'https://www.bbc.com/sport/football/47564048'
    #             },
    #             {
    #                 'title' : 'Sky Brown: The 10-year-old British skateboarder aiming to make history at Tokyo',
    #                 'URL' : 'https://www.bbc.com/sport/olympics/47523698'
    #             },
    #         ]
    #     }

