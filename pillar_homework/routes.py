from aiohttp import web


def setup_routes(app, handler):
    app.add_routes([
        web.get('/', handler.index)
    ])
