from aiohttp.web import Response, json_response

from pillar_homework.utils import parse_int


class Handler:

    def __init__(self, bcc_service):
        self._bcc_service = bcc_service

    @property
    def bcc_service(self):
        return self._bcc_service

    async def index(self, request):
        query_params = request.rel_url.query

        chapter_name = query_params.get('chapter')
        news_count = parse_int(query_params.get('news'))

        if (
            not chapter_name
            or news_count is None
            or news_count < 0
        ):
            return Response(status=400)

        parsed_data = await self.bcc_service.get_news(
            chapter_name=chapter_name,
            news_count=news_count,
        )

        if parsed_data is None:
            return Response(status=404)

        return json_response(parsed_data)
