import os, sys
sys.path.append(os.getcwd())

import pathlib
import asyncio

from aiohttp import web, ClientSession

from pillar_homework import routes
from pillar_homework.handler import Handler
from pillar_homework.bcc_service import BCCService
from pillar_homework.utils import load_config


PROJECT_ROOT = pathlib.Path(__file__).parent.parent


async def init(loop):
    config = load_config(
        file_name=f'{PROJECT_ROOT}/config/config.yaml'
    )

    client_session = ClientSession(loop=loop)
    bcc_service = BCCService(
        bcc_url=config['bcc_url'],
        client_session=client_session,
    )

    app = web.Application(loop=loop)

    handler = Handler(bcc_service=bcc_service)
    routes.setup_routes(app, handler)

    host = config['host']
    port = config['port']

    return app, host, port


def main():
    loop = asyncio.get_event_loop()
    app, host, port = loop.run_until_complete(init(loop))
    web.run_app(app, host=host, port=port)


if __name__ == '__main__':
    main()
