import yaml


def load_config(file_name):
    with open(file_name, 'rt') as f:
        data = yaml.load(f)

    return data


def parse_int(int_string):
    result = None

    if not int_string:
        return result

    try:
        result = int(int_string)
        return result
    except ValueError:
        pass

    return result
